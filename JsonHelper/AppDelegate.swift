//
//  AppDelegate.swift
//  JsonHelper
//
//  Created by Dmytro Golub on 29/11/2016.
//  Copyright © 2016 Dmytro Golub. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

