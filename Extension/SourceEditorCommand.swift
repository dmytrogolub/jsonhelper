//
//  SourceEditorCommand.swift
//  Extension
//
//  Created by Dmytro Golub on 29/11/2016.
//  Copyright © 2016 Dmytro Golub. All rights reserved.
//

import Foundation
import XcodeKit

class SourceEditorCommand: NSObject, XCSourceEditorCommand {
    
    func perform(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void ) -> Void {
        guard let selection = invocation.buffer.selections.firstObject as? XCSourceTextRange else {
            completionHandler(NSError(domain: "jsonformatter.xcode.error",
                                      code: -1,
                                      userInfo: [
                                        NSLocalizedDescriptionKey: NSLocalizedString("«Json Hepler» requires you to select json snippet  to work.", comment: "«Json Hepler» requires you to select json snippet  to work.")
                ]))
            return
        }        
        
        for index in selection.start.line ... selection.end.line {
            
            guard let line = invocation.buffer.lines[index] as? String else {
                continue
            }
            
            
            
            var newStr = line.replacingOccurrences(of: "\"", with: "\\\"")
            let trimmedLine = newStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            newStr = "\"" + trimmedLine + "\""
            var formattedString:NSString = newStr as NSString
            if index != selection.end.line {
                formattedString =  newStr.appending("+") as NSString
            }
            invocation.buffer.lines.replaceObject(at: index, with: formattedString)
            
            
        }
        
        
        completionHandler(nil)
    }
    
}
